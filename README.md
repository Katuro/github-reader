# Setup

ruby 2.6.3

Use a PostgreSQL database.

# Description

GithubReader get the data of a Github user with a Github login. It use the GraphQL API of Github, and only display public data from one year ago at most.


# Usage

You can also simply open this url : https://githubreader.herokuapp.com/
The app is heberged on Heroku.

If you want to start it on your own machine, follow the next steps :

You must first get your own Github Token.
For that, create or connect to your Github account.
Then go on this page : https://github.com/settings/tokens and generate one.

Add your token to your env.

On unix systems :
```bash
$> export GITHUB_TOKEN=yourtoken
```

You will have to do it every time you start a terminal. But you can also add it to your .bashrc file (or .zshrc, depending on what you use).

Go in the root directory of the project.

```bash
$> bundle install
$> rails s
```

Then open localhost:3000 in your browser.