class UsersController < ApplicationController
  require 'resolv-replace'

  def search
    user_login = params[:login]
    @user_data = ::UserSearch.get_user(user_login)

    return if @user_data.nil? || @user_data.user.nil?

    @total_commits_count = {}
    # Get the total count of commits made by a user in a repository
    @user_data.user.contributionsCollection.commitContributionsByRepository.each do |cr|
      # Get commits count of the repository
      commits_count = ::UserSearch.get_commits_count(cr.repository.owner.login, cr.repository.name)
      # Sort them by value
      # And stock them by repository name
      @total_commits_count[cr.repository.name] = commits_count.sort_by {|k,v| v}.reverse
    end

    @most_used_language = ::UserSearch.most_used_language(@user_data)

  end
end