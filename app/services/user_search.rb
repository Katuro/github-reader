class UserSearch

  def self.get_user(user_login)
    # Create the GraphQL query that will get the user login, name,
    # his contributions and the language used for it
    user_query = GQLi::DSL.query {
      user(login: user_login) {
        login
        name
        contributionsCollection(to: DateTime.now.to_s, from: (DateTime.now - 1.year).to_s) {
          commitContributionsByRepository {
            repository {
              name
              owner {
                login
              }
              url
              languages(first: 99) {
                edges {
                  node {
                    id
                    color
                    name
                  }
                }
              }
            }
          }
        }
      }
    }

    # Send this query to the Github GraphQL API
    response = GithubReader::Application.config.github_gkl.execute(user_query)
    # Return the call's response
    response&.data
  end

  def self.get_repository(repository_owner, repository_name)
    repoFragment = GQLi::DSL.fragment('RepoFragment', 'Repository') {
      refs(first: 100, refPrefix: 'refs/heads/') {
        edges {
          node {
            target {
              __on('Commit') {
                author {
                  user {
                    login
                  }
                }
                history(first: 0) {
                  totalCount
                }
              }
            }
          }
        }
      }
    }

    query = GQLi::DSL.query {
      repository(owner: repository_owner, name: repository_name) {
        ___ repoFragment
      }
    }

    response = GithubReader::Application.config.github_gkl.execute(query)
    response&.data
  end

  def self.get_commits_count(repository_owner, repository_name)
    repo = get_repository(repository_owner, repository_name)
    users_commits = {}

    repo.repository.refs.edges.each do |ref|
      user_login = ref.node.target.author&.user&.login || 'unknown'
      commits_count = ref.node.target.history.totalCount
      users_commits[user_login] = commits_count
    end

    users_commits
  end

  def self.most_used_language(user_data)
    used_language = Hash.new(-1)
    user_data.user.contributionsCollection.commitContributionsByRepository.each do |cr|
      # Iterate on each repository and then on each languages used to count how used they are
      cr.repository.languages.edges.each do |l|
        used_language[l.node.name] += 1
      end
    end
    # Sort by value, keep the 3 last and take only the name
    used_language.sort_by {|k,v| v}.reverse.first(3).map(&:first)
  end
end
