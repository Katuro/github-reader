Rails.application.routes.draw do
  root 'home#index'

  post '/users/search', to: 'users#search'
end
